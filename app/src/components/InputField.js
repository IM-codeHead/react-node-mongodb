import React from "react";
import ReactDOM from "react-dom";

export default class InputField extends React.Component {
  constructor(props) {
    super();
    this.state = {
      value: '',
    };
  }

  onChange(evt) {
    this.setState({ value: evt.target.value });
  }

  render() {
    return (<input type={this.props.type} name={this.props.label} onChange={this.onChange.bind(this)}/>);
  }
}
