import React from "react";
import ReactDOM from "react-dom";
import Button from "./Button.js";
import InputField from "./InputField.js";

export default class App extends React.Component {
  constructor(props) {
    super();
    this.state = {
      status: '',
      currentUsernamne: '',
      password: '',
    };
  }

  showCurrentUser() {
    const currentUsername = this.loginUname.state.value;
    const currentPassword = this.loginPwd.state.value;
    const userObj = {
      username: currentUsername,
      password: currentPassword,
    };
    const onComplete = (users) => {
      if (users.indexOf(userObj) >= 0) {
        const currentUser = users[users.indexOf(userObj)];
        this.setState({ currentUsernamne: currentUser.username, password: currentUser.password });
      } else {
        this.setState({ status: 'given user does not exist'});
      }
    }
    this.props.sendGetUsersReq(onComplete);
  }

  addNewUser() {
    const newUsername = this.signupUname.state.value;
    const newPassword = this.signupPwd.state.value;
    const userObj = {
      username: newUsername,
      password: newPassword,
    };
    const onComplete = (users) => {
      if (users.indexOf(userObj) < 0) {
        this.props.sendPostUsersReq(userObj);
      } else {
        this.setState({ status: 'user already exist'});
      }
    }
    this.props.sendGetUsersReq(onComplete);
  }

  render() {
    return (<div>
        <div>
          <span>Username:</span><InputField ref={(ref) => { this.signupUname = ref; }} label={'username'} type="text"/>
          <span>Password:</span><InputField ref={(ref) => { this.signupPwd = ref; }} label={'password'} type="password"/>
          <Button buttonType={'submit'} label={'SIGN UP'} onSubmit={this.addNewUser.bind(this)}/>
        </div>
        <div>
          <span>Username:</span><InputField ref={(ref) => { this.loginUname = ref; }} label={'username'} type="text"/>
          <span>Password:</span><InputField ref={(ref) => { this.loginPwd = ref; }} label={'password'} type="password"/>
          <Button buttonType={'submit'} label={'LOGIN'} onSubmit={this.showCurrentUser.bind(this)}/>
          <div>{this.state.currentUsernamne}</div>
        </div>
      </div>);
  }
}
