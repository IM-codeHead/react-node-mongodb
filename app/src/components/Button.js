import React from "react";
import ReactDOM from "react-dom";

export default class Button extends React.Component {
  constructor(props) {
    super();
    this.state = {};
  }

  onSubmit(event) {
    this.props.onSubmit(event);
  }

  render() {
    return (<button
        className={`button-${this.props.buttonType}`}
        onClick={this.onSubmit.bind(this)}
      >
        {this.props.label}
      </button>);
  }
}
