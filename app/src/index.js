import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";


const xhr = new XMLHttpRequest();
xhr.withCredentials = true;
let tableData = null;
let appRef = null;
let onComplete = () => {};

// Sending request to node server
xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4 && this.responseText !== '') {
    const responseType = JSON.parse(this.responseText).responseType;
    if (responseType === 'sent') {
      const users = JSON.parse(this.responseText).users;
      onComplete(users);
    }
  }
});

const sendGetUsersReq = (callback) => {
  onComplete = callback;
  xhr.open("GET", "http://localhost:3000");
  xhr.send(null);
};

const sendPostUsersReq = (body) => {
  xhr.open("POST", "http://localhost:3000");
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.setRequestHeader("cache-control", "no-cache");
  xhr.send(JSON.stringify(body));
}

const sendDeleteUsersReq = (body) => {
  xhr.open("DELETE", "http://localhost:3000");
  xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xhr.setRequestHeader("cache-control", "no-cache");
  xhr.send(JSON.stringify(body));
}

ReactDOM.render(<App
    sendGetUsersReq={sendGetUsersReq}
    sendPostUsersReq={sendPostUsersReq}
    sendDeleteUsersReq={sendDeleteUsersReq}
    ref={(ref) => { appRef = ref }}
  />
, document.getElementById("app"));
