const express = require('express');
const http = require('http');
const request = require('request');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const uri = "mongodb+srv://Aniket:aniket123@sample-idyt8.mongodb.net";
const mongoClient = new MongoClient(uri, { useNewUrlParser: true });

const app = express();
let count = 0;

app.use(bodyParser.json());

app.use(function(request, response, next) {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, cache-control, Accept");
  next();
});

app.get('/', (request, response) => {
  mongoClient.connect((err, db) => {
    if (err) throw err;
    db.db("sample-db").collection("users").find().toArray().then((users) => {
      const resObj = { responseType: "sent", users: users };
      response.send(JSON.stringify(resObj));
      db.close();
    });
  });
});

app.post('/', (request, response) => {
  const user = { username: request.body.username, password: request.body.password };
  mongoClient.connect((err, db) => {
    if (err) throw err;
    const dbObj = db.db("sample-db");
    dbObj.collection("users").insertOne(user, function(err, res) {
      if (err) throw err;
      const resObj = { responseType: "added" };
      response.send(JSON.stringify(resObj));
      db.close();
    });
  });
});

app.delete('/', (request, response) => {
  mongoClient.connect((err, db) => {
    if (err) throw err;
    const dbObj = db.db("sample-db");
    const user = request.body;
    dbObj.collection("users").deleteOne(user, function(err, res) {
      if (err) throw err;
      const resObj = { responseType: "removed" };
      response.send(JSON.stringify(resObj));
      db.close();
    });
  });
});

const hostname = "localhost";
const port = 3000;

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
